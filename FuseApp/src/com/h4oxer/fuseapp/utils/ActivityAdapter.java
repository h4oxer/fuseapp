package com.h4oxer.fuseapp.utils;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.h4oxer.fuseapp.R;
import com.h4oxer.fuseapp.models.Event;
import com.h4oxer.fuseapp.models.EventDay;
/**
 * Een list adapter om activity's te parsen en weer te geven.
 * @author Incalza Dario
 *
 */
public class ActivityAdapter extends BaseExpandableListAdapter {
	
	private final Context c;
	private final List<EventDay> events;
	
	public ActivityAdapter(Context c, List<EventDay> events){
		this.c = c;
		this.events = events;
	}
	
	@Override
	public Event getChild(int arg0, int arg1) {
		List<Event> eventsDay = events.get(arg0).getEvents();
		return eventsDay.get(arg1);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		 
		  Event detailInfo = (Event) getChild(groupPosition, childPosition);
		  if (view == null) {
		   LayoutInflater infalInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		   view = infalInflater.inflate(R.layout.child_row, null);
		  }
		   
		  TextView sequence = (TextView) view.findViewById(R.id.eventTitle);
		  sequence.setText(detailInfo.getTitle().trim());
		  Typeface face=Typefaces.get(c,"fonts/Roboto-Light.ttf");
		  sequence.setTypeface(face);
		  Typeface face2 = Typefaces.get(c, "fonts/Roboto-Regular.ttf");
		  //sequence.setText("title");
		  TextView childItem = (TextView) view.findViewById(R.id.eventPlace);
		  childItem.setText(detailInfo.getLocation().trim());
		  childItem.setTypeface(face2);
		  //childItem.setText("place");
		  TextView time = (TextView) view.findViewById(R.id.eventTime);
		  time.setText(detailInfo.getDate().trim());
		  time.setTypeface(face2);
		   
		  return view;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		
		return events.get(groupPosition).getEvents().size();
	}

	@Override
	public EventDay getGroup(int groupPosition) {
		return events.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return events.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View view, ViewGroup parent) {
		EventDay headerInfo = (EventDay) getGroup(groupPosition);
		  if (view == null) {
		   LayoutInflater inf = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		   view = inf.inflate(R.layout.group_heading, null);
		  }
		   
		  TextView heading = (TextView) view.findViewById(R.id.heading);
		  heading.setText(headerInfo.getDay().trim());
		  Typeface face=Typefaces.get(c,"fonts/Roboto-CondensedItalic.ttf");
		  heading.setTypeface(face);
		   
		  return view;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
