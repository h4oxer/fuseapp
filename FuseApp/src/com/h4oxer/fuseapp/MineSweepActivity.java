package com.h4oxer.fuseapp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import net.sourceforge.zbar.Symbol;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.h4oxer.fuseapp.models.Block;
import com.h4oxer.fuseapp.utils.Typefaces;
/**
 * Stelt het minesweeping screen voor. Layout is nog niet goed
 * 
 * TODO: layout fixen duh
 * 
 * @author Incalza Dario & Jacobs Tom
 *
 */
public class MineSweepActivity extends Activity {

	private TextView txtDistance;

	//private TableLayout mineField; // table layout to add mines to
	
	private GridView mineField;
	private List<Block> blocksList = null;
	private Block blocks[][]; // blocks for mine field
	private int prizeColumn = -1;
	private int prizeRow = -1;
	
	private int numberOfRowsInMineField = 8;
	private int numberOfColumnsInMineField = 8;
	private int totalNumberOfMines = 10;

	private boolean areMinesSet; // check if mines are planted in blocks
	private boolean isGameOver;
	private int minesToFind; // number of mines yet to be discovered
	
	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	
	/**
	 * Initialize shit
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.minesweep_layout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Typeface face=Typefaces.get(this,"fonts/digital-7.ttf");
		txtDistance = (TextView) findViewById(R.id.Distance);
		txtDistance.setTypeface(face);
		((TextView) findViewById(R.id.Description)).setTypeface(face);
		Button newGame = (Button) findViewById(R.id.onNewGame);
		newGame.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MineSweepActivity.this.endExistingGame();
				MineSweepActivity.this.startNewGame();
				
			}
		});
		
		Button newScan = (Button) findViewById(R.id.onScan);
		newScan.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				launchQRScanner();
				
			}
		});
		
		Button help = (Button) findViewById(R.id.onHelp);
		help.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showHelpDialog();
			}
		});
		
		// set font style for timer and mine count to LCD style
		
		mineField = (GridView) findViewById(R.id.MineField);
		startNewGame();
	}
	
	/**
	 * Notifier voor QR data
	 */
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_SCANNER_REQUEST:
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {
                	try{
	                    //Toast.makeText(this, "Scan Result = " + data.getStringExtra(ZBarConstants.SCAN_RESULT), Toast.LENGTH_SHORT).show();
	                	String scanResult = data.getStringExtra(ZBarConstants.SCAN_RESULT);
	                	String coordsString = scanResult.split("!")[1];
	                	String[] coords = coordsString.split(",");
	                	int currentRow= Integer.parseInt(coords[0]);
	                	int currentColumn = Integer.parseInt(coords[1]);
	                	clickBlock(currentColumn,currentRow);
                	}catch(ArrayIndexOutOfBoundsException e){
                		Toast.makeText(this, "Woops something went wrong, please scan again !", Toast.LENGTH_LONG).show();
                	}catch(NumberFormatException e){
                		Toast.makeText(this, "Changes are the QR code is corrupt. Please complain !", Toast.LENGTH_LONG).show();
                	}
                } else if(resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Y u no scan ?!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
	
	/**
	 * Launch qr scanner
	 */
	private void launchQRScanner() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            intent.putExtra(ZBarConstants.SCAN_MODES, new int[]{Symbol.QRCODE});
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, "Rear Facing Camera Unavailable", Toast.LENGTH_SHORT).show();
        }
    }
	
	/**
	 * Check voor een camera, kan handig zijn
	 * @return
	 */
	private boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }


	private void startNewGame()
	{
		// plant mines and do rest of the calculations
		createMineField();
		// display all blocks in UI
		showMineField();
		showTutorial();
		Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
		edit.putBoolean("firstGame", false);
		edit.commit();
		minesToFind = totalNumberOfMines;
		setGameOver(false);
	}

	/**
	 * 
	 */
	private void showTutorial() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if(prefs.getBoolean("firstGame", true)){
			showHelpDialog();
		}
	}

	/**
	 * 
	 */
	private void showHelpDialog() {
		LinearLayout dialogLayout = new LinearLayout(this);
		TextView message = new TextView(this);
		Typeface face=Typefaces.get(this,"fonts/Roboto-Regular.ttf");
		message.setText("Play our game ! This game is played like your classic minesweeper game. Try to find the green cross to win a prize while they last. Use the info 'distance to prize' that tells you how many squares you're away from winning a prize. Play the game by scanning QR codes 6 and 7 May @Alma3. A square can be uncovered by scanning the respective QR code on our life size minefield !");
		message.setTypeface(face);
		message.setTextColor(Color.WHITE);
		dialogLayout.addView(message);
	    dialogLayout.setOrientation(LinearLayout.VERTICAL);
	    dialogLayout.setPadding(15, 15, 15, 15);
	    ScrollView scrollPane = new ScrollView(this);
	    scrollPane.setVerticalScrollBarEnabled(true);
	    scrollPane.addView(dialogLayout);
		Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Tutorial");
		builder.setView(scrollPane);
		final AlertDialog aDialog = builder.create();
		aDialog.setCancelable(false);
		aDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Ok, got it !", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				aDialog.dismiss();
				
			}
		});
		aDialog.show();
	}
	
	/**
	 * Aangepast naar gridview. Alle blocks in block[][] worden weergegeven. (buiten 0-de en laatste block volgens conventie)
	 */
	private void showMineField()
	{	
		blocksList = new ArrayList<Block>();
		// remember we will not show 0th and last Row and Columns
		// they are used for calculation purposes only
		for (int row = 1; row < numberOfRowsInMineField + 1; row++)
		{	
			for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
			{	
				this.blocks[row][column].setClickable(false);
				blocksList.add(this.blocks[row][column]);
			}
			
		}
		
		BaseAdapter adapter = new BaseAdapter() {
			
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				
				return blocksList.get(position);
			}
			
			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Object getItem(int position) {
				return blocksList.get(position);
			}
			
			@Override
			public int getCount() {
				return blocksList.size();
			}
		};
		mineField.setAdapter(adapter);
	}

	private void endExistingGame()
	{
		txtDistance.setText("000"); // revert all text
		
		mineField.setAdapter(null);
		
		// set all variables to support end of game
		areMinesSet = false;
		setGameOver(false);
		minesToFind = 0;
	}
	
	private void clickBlock(int currentRow,int currentColumn){
		// set mines on first click
		if (!areMinesSet)
		{
			areMinesSet = true;
			setMines(currentRow, currentColumn);
		}
		
		txtDistance.setText("" + (int) Math.round(Math.sqrt(Math.pow(currentRow - prizeRow,2) + Math.pow(currentColumn - prizeColumn,2))));

		// this is not first click
		// uncover nearby blocks
		// till we get numbered mines
		// open nearby blocks till we get numbered blocks
		rippleUncover(currentRow, currentColumn);

		// did we clicked a mine
		if (blocks[currentRow][currentColumn].hasMine())
		{
			// Oops, game over
			finishGame(currentRow,currentColumn);
		}
		
		if(blocks[currentRow][currentColumn].isPrize()){
			blocks[currentRow][currentColumn].setPrizeIcon(false);
			winGame();
		}

		// check if we win the game
		if (checkGameWin())
		{
			// mark game as win
			winGame();
		}
	}

	private void createMineField()
	{
		// we take one row extra row for each side
		// overall two extra rows and two extra columns
		// first and last row/column are used for calculations purposes only
		//	 x|xxxxxxxxxxxxxx|x
		//	 ------------------
		//	 x|              |x
		//	 x|              |x
		//	 ------------------
		//	 x|xxxxxxxxxxxxxx|x
		// the row and columns marked as x are just used to keep counts of near by mines

		blocks = new Block[numberOfRowsInMineField + 2][numberOfColumnsInMineField + 2];
		int counter = 1;
		for (int row = 0; row < numberOfRowsInMineField + 2; row++)
		{
			for (int column = 0; column < numberOfColumnsInMineField + 2; column++)
			{	
				blocks[row][column] = new Block(this);
				blocks[row][column].setDefaults(counter);
				counter++;

				// pass current row and column number as final int's to event listeners
				// this way we can ensure that each event listener is associated to 
				// particular instance of block only
				final int currentRow = row;
				final int currentColumn = column;

				// add Click Listener
				// this is treated as Left Mouse click
				blocks[row][column].setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View view)
					{

						clickBlock(currentRow,currentColumn);
					}
				});
			}
		}
	}

	private boolean checkGameWin()
	{
		for (int row = 1; row < numberOfRowsInMineField + 1; row++)
		{
			for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
			{
				if (!blocks[row][column].hasMine() && blocks[row][column].isCovered())
				{
					return false;
				}
			}
		}
		return true;
	}

	private void updateMineCountDisplay()
	{
		if (minesToFind < 0)
		{
		}
		else if (minesToFind < 10)
		{
		}
		else if (minesToFind < 100)
		{
		}
		else
		{
		}
	}

	private void winGame()
	{
		setGameOver(true);
		minesToFind = 0; //set mine count to 0

		//set icon to cool dude
		updateMineCountDisplay(); // update mine count

		// disable all buttons
		// set flagged all un-flagged blocks
		for (int row = 1; row < numberOfRowsInMineField + 1; row++)
		{
			for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
			{
				blocks[row][column].setClickable(false);
				if (blocks[row][column].hasMine())
				{
					blocks[row][column].setBlockAsDisabled(false);
					blocks[row][column].setMineIcon(false);
				}
			}
		}
		blocks[prizeRow][prizeColumn].setPrizeIcon(false);

		// show message
		Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("You Win!");
		builder.setMessage("You've earned your prize! Go to the FUSE Bar and have someone click the button below to claim your prize, while they last !\n\nTimestamp:\n"+ DateFormat.getTimeInstance().format(Calendar. getInstance().getTime()));
		final AlertDialog aDialog = builder.create();
		aDialog.setCancelable(false);
		aDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Claim Prize!", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				aDialog.dismiss();
				
			}
		});
		aDialog.show();
	}

	private void finishGame(int currentRow, int currentColumn)
	{
		setGameOver(true); // mark game as over
		// show all mines
		// disable all blocks
		for (int row = 1; row < numberOfRowsInMineField + 1; row++)
		{
			for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
			{
				// disable block
				blocks[row][column].setClickable(false);
				
				// block has mine and is not flagged
				if (blocks[row][column].hasMine())
				{
					// set mine icon
					blocks[row][column].setMineIcon(false);				
					blocks[row][column].setBlockAsDisabled(false);

				}
			}
		}

		// trigger mine
		blocks[currentRow][currentColumn].triggerMine();
		blocks[prizeRow][prizeColumn].setPrizeIcon(false);
		// show message
		Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Game over..");
		WebView wv = new WebView(this);
		builder.setView(wv);
		wv.loadUrl("file:///android_asset/explosion.gif");
		AlertDialog aDialog = builder.create();
		aDialog.show();
		showDialog("You FAILED!", 1000);
	}


	private void setMines(int currentRow, int currentColumn)
	{
		// set mines excluding the location where user clicked
		Random rand = new Random();
		int mineRow, mineColumn;

		for (int row = 0; row < totalNumberOfMines; row++)
		{
			mineRow = rand.nextInt(numberOfColumnsInMineField);
			mineColumn = rand.nextInt(numberOfRowsInMineField);
			if ((mineRow + 1 != currentColumn) || (mineColumn + 1 != currentRow))
			{
				if (blocks[mineColumn + 1][mineRow + 1].hasMine())
				{
					row--; // mine is already there, don't repeat for same block
				}
				// plant mine at this location
				blocks[mineColumn + 1][mineRow + 1].plantMine();
			}
			// exclude the user clicked location
			else
			{
				row--;
			}
		}

		//Set the prize somewhere else too - LOOL @ Code duplication
		boolean settingPrize = true;
		while(settingPrize){
			prizeColumn = rand.nextInt(numberOfColumnsInMineField);
			prizeColumn++;
			prizeRow = rand.nextInt(numberOfRowsInMineField);
			prizeRow++;
			// exclude the user clicked location
			if ((prizeColumn + 1 != currentColumn) || (prizeRow + 1 != currentRow))
			{
				if (!blocks[prizeRow][prizeColumn].hasMine())
				{
					blocks[prizeRow][prizeColumn].setPrize(true);
					settingPrize = false;
				}
			}
		}


		int nearByMineCount;

		// count number of mines in surrounding blocks 
		for (int row = 0; row < numberOfRowsInMineField + 2; row++)
		{
			for (int column = 0; column < numberOfColumnsInMineField + 2; column++)
			{
				// for each block find nearby mine count
				nearByMineCount = 0;
				if ((row != 0) && (row != (numberOfRowsInMineField + 1)) && (column != 0) && (column != (numberOfColumnsInMineField + 1)))
				{
					// check in all nearby blocks
					for (int previousRow = -1; previousRow < 2; previousRow++)
					{
						for (int previousColumn = -1; previousColumn < 2; previousColumn++)
						{
							if (blocks[row + previousRow][column + previousColumn].hasMine())
							{
								// a mine was found so increment the counter
								nearByMineCount++;
							}
						}
					}

					blocks[row][column].setNumberOfMinesInSurrounding(nearByMineCount);
				}
				// for side rows (0th and last row/column)
				// set count as 9 and mark it as opened
				else
				{
					blocks[row][column].setNumberOfMinesInSurrounding(9);
					blocks[row][column].OpenBlock();
				}
			}
		}
	}

	private void rippleUncover(int rowClicked, int columnClicked)
	{
		// don't open flagged or mined rows
		if (blocks[rowClicked][columnClicked].hasMine())
		{
			return;
		}

		// open clicked block
		blocks[rowClicked][columnClicked].OpenBlock();

		// if clicked block have nearby mines then don't open further
		if (blocks[rowClicked][columnClicked].getNumberOfMinesInSorrounding() != 0 )
		{
			return;
		}

		// open next 3 rows and 3 columns recursively
		for (int row = 0; row < 3; row++)
		{
			for (int column = 0; column < 3; column++)
			{
				// check all the above checked conditions
				// if met then open subsequent blocks
				if (blocks[rowClicked + row - 1][columnClicked + column - 1].isCovered()
						&& (rowClicked + row - 1 > 0) && (columnClicked + column - 1 > 0)
						&& (rowClicked + row - 1 < numberOfRowsInMineField + 1) && (columnClicked + column - 1 < numberOfColumnsInMineField + 1))
				{
					rippleUncover(rowClicked + row - 1, columnClicked + column - 1 );
				} 
			}
		}
		return;
	}
	
	private void showDialog(String message, int milliseconds)
	{
		// show message
		Toast dialog = Toast.makeText(
				getApplicationContext(),
				message,
				Toast.LENGTH_LONG);

		dialog.setGravity(Gravity.CENTER, 0, 0);
		dialog.setDuration(milliseconds);
		dialog.show();
	}

	/**
	 * @return the isGameOver
	 */
	public boolean isGameOver() {
		return isGameOver;
	}

	/**
	 * @param isGameOver the isGameOver to set
	 */
	public void setGameOver(boolean isGameOver) {
		this.isGameOver = isGameOver;
	}
	
	@Override
	protected void onDestroy(){
		if(this.blocksList != null){
			try{
				FileOutputStream fos = this.openFileOutput("blocksSavedState.db", Context.MODE_PRIVATE);
				ObjectOutputStream os = new ObjectOutputStream(fos);
				os.flush();
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		super.onDestroy();
	}
}
