package com.h4oxer.fuseapp.db;

import java.util.ArrayList;
import java.util.List;

import com.h4oxer.fuseapp.models.Event;
import com.h4oxer.fuseapp.models.EventDay;
/**
 * 
 * Static singleton event data.
 * 
 * @author Incalza Dario
 *
 */
public class ActivityDb {
	
	private static List<EventDay> events = null;
	
	
	public static List<EventDay> getEvents(){
		if(events == null){
			events = new ArrayList<EventDay>();
			
			//events sunday
			List<Event> sundayEvents = new ArrayList<Event>();
			sundayEvents.add(new Event("Receptie","Fancy hapjes en gratis champagne zorgen voor een feestelijke opening van deze awesome kiesweek !","Alma 3","19h-22h"));
			sundayEvents.add(new Event("Fakfeestje", "Om deze geweldige kiesweek van start te laten gaan, zetten we onze fak in vuur en vlam! Burning shots aan 1 euro, veel vuur en vonkjes en zot veel FUSE-sfeer ... BE THERE! ", "'t Elixir", "22h"));
			events.add(new EventDay("ZONDAG 5 MEI", sundayEvents));
			
			//events monday
			List<Event> mondayEvents = new ArrayList<Event>();
			mondayEvents.add(new Event("Cava-ontbijt", "Hoe de saaie lesdag beter inzetten dan met een uitgebreid cava-ontbijt en VERSE koffiekoeken?!", "Alma 3", "7h-10h"));
			mondayEvents.add(new Event("Record Randanimatie", "Kom ook dinsdagnamiddag zeker het grasveldje voor alma 3 checken. Er staat dan een mega awesome indoor lasershooting van ExxonMobil, een rodeokameel, airhockey, beer pong en een springkasteel!", "Alma 3", "11h-19h"));
			mondayEvents.add(new Event("Croques", "Afspraak maandagmiddag aan de Alma voor croques aan 0,50 euro! Denk al vast na over je beleg. Wordt het gerookte zalm met kruidenkaas? Bolognaisesaus? Ananas? Of toch liever tomaat, mozzarella en pesto? ", "Alma 3", "12h-14h"));
			mondayEvents.add(new Event("Barbecue", "Lekkere vleesjes, verse groentjes en aardappelsalade op oma's wijze en dat allemaal tegen studentikoze prijzen! Dat zijn de ingredi�nten voor een overheerlijke BBQ !", "Alma 3", "18h-20h"));
			mondayEvents.add(new Event("Lounge", "Na een dag vol activiteiten kan je uitblazen op onze lounge avond met live jazz en muziek van Atmosfeer. Pokersets en andere gezelschapspelen aanwezig!", "Alma 3", "20h-23h"));
			events.add(new EventDay("MAANDAG 6 MEI",mondayEvents));
			
			//events tuesday
			List<Event> tuesdayEvents = new ArrayList<Event>();
			tuesdayEvents.add(new Event("Brunch", "Beste manier om een dag te beginnen is een brunch natuurlijk. Tot 's middags ontbijt? Beter kan het toch niet worden!", "Alma 3", "10h-12h"));
			tuesdayEvents.add(new Event("Adventure time !", "Ben jij beter als Indiana Jones, kom het bewijzen !", "Alma 3", "13h-19h"));
			tuesdayEvents.add(new Event("RedBull Defender","Kom chillen en een Redbull drinken bij onze heuze Redbull Defender !","Alma 3","13h-19h"));
			tuesdayEvents.add(new Event("Pudding", "Kom genieten voor de les of tijdens de leswisseling van heerlijk zelfgemaakte pudding! Zeker de moeite!", "Alma 3", "14h-16h"));
			tuesdayEvents.add(new Event("ExxonMobil LaserShoot", "Spreek met vrienden af en kom elkaar bestoken met lasers in onze awesome ExxonMobil Lasershoot !", "Alma 3", "14h-18h"));
			tuesdayEvents.add(new Event("Wraps", "Een gezond tussendoortje of een volwaardig avondmaal? Het kan, met wraps!", "Alma 3", "18h-20h"));
			tuesdayEvents.add(new Event("Tick Tick Boom Cantus","Dinsdagavond sluiten we af met een KNALLER van formaat: de TICK TICK BOOM CANTUS! Het concept is eenvoudig: wij zorgen voor liters bier en u zingt de longen uit uw lijf. Omdat het bij FUSE ietsje meer mag zijn, voegen we er nog een aantal explosieve bommen aan toe! Benieuwd wanneer deze afgaan? En of u er heelhuids vanaf komt? Om dit te weten te komen is er maar ��n oplossing: rep je op dinsdag 7 mei om 20u naar de Waaiberg (Io vivat om 20u30) en beleef de vuurigste cantus aller tijden! Dit alles voor maar 8� (13� voor niet leden). Geen reden dus om thuis te blijven! Huppel vanaf maandagmiddag naar het theokot om je kaart te reserveren. Doe het snel, cause time is ticking�","Waaiberg","20h-23h"));
			events.add(new EventDay("DINSDAG 7 MEI",tuesdayEvents));
			
			//events wednesday
			List<Event> wednesdayEvents = new ArrayList<Event>();
			wednesdayEvents.add(new Event("Lolploegen", "Fuse gaat rusten, en laat het roer over aan de lolploegen.", "Alma 3", "12h-22h"));
			wednesdayEvents.add(new Event("Kiescantus", "Er is niets beter dan een kiesweek afsluiten met een kiescantus. Kom dat glas heffen!", "De Waaiberg", "21h-1h"));
			events.add(new EventDay("WOENSDAG 8 MEI", wednesdayEvents));
			
			//events thursday
			List<Event> thursdayEvents = new ArrayList<Event>();
			thursdayEvents.add(new Event("Stemmen !", "Op www.vtk.be kan u uw stem uitbrengen tussen 10u00 en 19u00. Dat de beste moge winnen!", "www.vtk.be", "10h-19h"));
			events.add(new EventDay("DONDERDAG 9 MEI", thursdayEvents));
		}
		
		return events;
	}

}
