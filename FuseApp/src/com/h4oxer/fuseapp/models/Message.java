/**
 * 
 */
package com.h4oxer.fuseapp.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * 
 * Een push message.
 * 
 * @author Incalza Dario
 *
 */
public class Message implements Comparable<Message>{
	
	public final static String TIMESTAMP_PATTERN = "E dd/MM hh:mm";
	
	private final String subject,title;
	private final Date timestamp;
	private int id;
	
	public Message(String title,String subject,Date timestamp){
		
		this.title = title;
		this.subject = subject;
		this.timestamp = timestamp;
		
	}

	public String getSubject() {
		return subject;
	}
	
	public String getTimeStampAsString(){
		SimpleDateFormat ft = new SimpleDateFormat (TIMESTAMP_PATTERN,Locale.getDefault());
		long currentTime = System.currentTimeMillis();
		long difference = currentTime - this.timestamp.getTime();
		int seconds = (int) (difference / 1000) % 60 ;
		int minutes = (int) ((difference / (1000*60)) % 60);
		if(difference < 3600000){
			return (minutes+" min "+seconds+" s geleden" );
		}
		return ft.format(this.timestamp);
	}
	
	public String getTitle() {
		return title;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	/**
	 * Parse a string to a message object. Valid string example [COMEDYNIGHT]-[Gratis Bier !]-[Iedere toeschouwer krijgt een gratis pintje !]
	 * @param source
	 * @return
	 */
	public final static Message parseFromString(String source){
		//TODO:finish
		String[] elements = source.split("-");
		if(elements.length != 3){
			throw new IllegalArgumentException("Not a valid source message.");
		}
		
		return null;
		
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Message another) {
		return another.getTimestamp().compareTo(this.getTimestamp());
	}
	
	@Override
	public boolean equals(Object other){
		Message m = null;
		try{
			m = (Message) other;
		}catch(ClassCastException e){
			return false;
		}
		
		return (this.subject.equals(m.getSubject()) && this.title.equals(m.getTitle()) && this.getTimestamp().equals(m.getTimestamp()));
	}
	
	@Override
	public String toString(){
		return (this.title+"-"+this.subject+"-"+this.getTimeStampAsString());
	}
}
