package com.h4oxer.fuseapp.models;

import java.util.List;
/**
 * Een bepaalde dag met events.
 * @author Incalza Dario
 *
 */
public class EventDay {
	
	private final String day;
	private final List<Event> events;
	
	public EventDay(String day,List<Event> events){
		this.day = day;
		this.events = events;
	}

	public String getDay() {
		return day;
	}

	public List<Event> getEvents() {
		return events;
	}

}
