package com.h4oxer.fuseapp;

import java.util.Locale;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.h4oxer.fuseapp.utils.Typefaces;
/**
 * Stelt main chizzle voor..
 * 
 * @author Incalza Dario
 *
 */
public class MainActivity extends TabActivity {
	private static String[] values;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
  
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        final String regId = GCMRegistrar.getRegistrationId(this);
        if (regId.equals("")) {
          GCMRegistrar.register(this, GCMIntentService.SENDER_ID);
        } else {
        	//ignore
        }
        ImageView iv = (ImageView) findViewById(R.id.mobilead);
        iv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String url = "http://www.esso.be/Benelux-English/default.aspx";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
        initTabHost();
        
        
    }



	/**
	 * 
	 */
	private void initTabHost() {
		TabHost tabHost = getTabHost();
        TabSpec notifspec = tabHost.newTabSpec("Meldingen");
        notifspec.setIndicator(getTabIndicator("Meldingen"));
        //notifspec.setIndicator("Meldingen", getResources().getDrawable(R.drawable.tab_indicator));
        Intent notifIntent = new Intent(this, NotificationActivity.class);
        notifspec.setContent(notifIntent);
 
       
        TabSpec calendarspec = tabHost.newTabSpec("Activiteiten");
        calendarspec.setIndicator(getTabIndicator("Activiteiten"));
        //calendarspec.setIndicator("Activiteiten", getResources().getDrawable(android.R.drawable.ic_menu_agenda));
        Intent calendarIntent = new Intent(this, CalendarActivity.class);
        calendarspec.setContent(calendarIntent);
 
             
        TabSpec gamespec = tabHost.newTabSpec("Game");
        gamespec.setIndicator(getTabIndicator("Game"));
        //gamespec.setIndicator("Game", getResources().getDrawable(R.drawable.icon));
        Intent gameIntent = new Intent(this, MineSweepActivity.class);
        gamespec.setContent(gameIntent);
 
        // Adding all TabSpec to TabHost
        tabHost.addTab(notifspec); 
        tabHost.addTab(calendarspec);
        tabHost.addTab(gamespec);
	}
    

    private View getTabIndicator(String title) {
        View view = LayoutInflater.from(this).inflate(R.layout.tab_layout, null);
        TextView tv = (TextView) view.findViewById(R.id.textView);
        Typeface face=Typefaces.get(this,"fonts/Roboto-Regular.ttf");
        tv.setText(title.toUpperCase(Locale.getDefault()));
        tv.setTypeface(face);
        return view;
    }

    
}
