package com.h4oxer.fuseapp.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.h4oxer.fuseapp.models.Message;
/**
 * Database controller
 * @author Incalza Dario
 *
 */
public class MessageDataSource {
	
	private final DbHelper dbHelper;
	private SQLiteDatabase database;
	private static String[] allMessageColumns = new String[]{DbHelper.COLUMN_ID,DbHelper.COLUMN_TITLE,DbHelper.COLUMN_BODY,DbHelper.COLUMN_DATE};
	
	public MessageDataSource(final Context c){
		this.dbHelper = new DbHelper(c);
	}
	
	public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	}
	  
	public void close() {
		dbHelper.close();
	}
	
	public synchronized void createMessage(String title,String body,Date date){
		database.beginTransaction();
		ContentValues values = new ContentValues();
		values.put(DbHelper.COLUMN_TITLE, title);
		values.put(DbHelper.COLUMN_BODY, body);
		values.put(DbHelper.COLUMN_DATE, date.getTime());
		if(database.isOpen()){
			database.insertWithOnConflict(DbHelper.TABLE_MESSAGES, null,values,SQLiteDatabase.CONFLICT_IGNORE);
			database.setTransactionSuccessful();
		}
		database.endTransaction();
		
	}
	
	public synchronized List<Message> getMessages(){
		List<Message> messages = new ArrayList<Message>();
	    Cursor cursor = database.query(DbHelper.TABLE_MESSAGES,allMessageColumns, null, null, null, null, null);
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	try{
		      Message m = cursorToMessage(cursor);
		      messages.add(m);
		      cursor.moveToNext();
	    	}catch(ParseException e){
	    		//rather impossible to reach here
	    	}
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    Collections.sort(messages);
	    return messages;
		
	}

	private Message cursorToMessage(Cursor cursor) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(cursor.getLong(3));
		Date d = cal.getTime();
		return new Message(cursor.getString(1),cursor.getString(2),d);
	}
	
	

}
