package com.h4oxer.fuseapp;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.plus.GooglePlusUtil;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusShare;
import com.h4oxer.fuseapp.db.ActivityDb;
import com.h4oxer.fuseapp.models.Event;
import com.h4oxer.fuseapp.models.EventDay;
import com.h4oxer.fuseapp.utils.ActivityAdapter;
import com.h4oxer.fuseapp.utils.Typefaces;

/**
 * List van activiteiten 
 * 
 * @author Incalza Dario
 *
 */
public class CalendarActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {
	
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
	
	public static final int FACEBOOKSHARE = 0;
	public static final int TWITTERSHARE = 1;
	public static final int GOOGLEPLUSSHARE = 2;
	public static final int MOREINFO = 3;
	
	private final String fuseUrl = "http://fuse.vtk.be";
	private PlusClient mPlusClient;
	private ConnectionResult mConnectionResult;
	private ProgressDialog mConnectionProgressDialog;
	private QuickAction mQuickAction;
	
	private Event currentEvent;
	private EventDay currentDay;
	
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.h4oxer.fuseapp.R.layout.activity_calendar);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		mPlusClient = new PlusClient.Builder(this,this,this)
        .setVisibleActivities("http://schemas.google.com/AddActivity", "http://schemas.google.com/BuyActivity")
        .build();
		ActivityAdapter adapter = new ActivityAdapter(getApplicationContext(), ActivityDb.getEvents());
		ExpandableListView list = (ExpandableListView) findViewById(com.h4oxer.fuseapp.R.id.myList);
		list.setAdapter(adapter);
		list.setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				
				final EventDay d = ActivityDb.getEvents().get(groupPosition);
				final Event e = d.getEvents().get(childPosition);
				currentEvent = e;
				currentDay = d;
				//setup the action item click listener
				mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
					@Override
					public void onItemClick(QuickAction quickAction, int pos, int actionId) {
						
						if (actionId == MOREINFO) {
							showMoreInfo(e);
						} else {
							if(actionId == TWITTERSHARE){
								shareTwitter(e, d.getDay());
							}else if(actionId == GOOGLEPLUSSHARE){
								shareGooglePlus(e, d.getDay());
							}else if(actionId == FACEBOOKSHARE){
								shareFacebook();
							}
						}
					}

					
				});
				mQuickAction.show(v);
				mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_CENTER);
                return true;
			}
			
			private void showMoreInfo(Event e){
				final Dialog dialog = new Dialog(CalendarActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().getAttributes().windowAnimations = com.h4oxer.fuseapp.R.style.DialogAnimation;
                dialog.setContentView(com.h4oxer.fuseapp.R.layout.dialog_layout);
                //dialog.setTitle(e.getTitle().toUpperCase(Locale.getDefault()));
                dialog.setCancelable(true);
                //there are a lot of settings, for dialog, check them all out!
 
                //set up text
                TextView text = (TextView) dialog.findViewById(com.h4oxer.fuseapp.R.id.description);
                text.setText(e.getDescription());
                Typeface face=Typefaces.get(CalendarActivity.this,"fonts/Roboto-Light.ttf");
                text.setTypeface(face);
                
 
                //set up button
                Button button = (Button) dialog.findViewById(com.h4oxer.fuseapp.R.id.confirm);
                button.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						dialog.cancel();
						
					}
				});
                //now that the dialog is set up, it's time to show it    
                dialog.show();
			}
		});
		
		initQuickActionFunctionality();
	}
	
	
	private void initQuickActionFunctionality(){
		 ActionItem fbItem 		= new ActionItem(FACEBOOKSHARE, "Facebook", getResources().getDrawable(com.h4oxer.fuseapp.R.drawable.facebook));
		 ActionItem googleItem 	= new ActionItem(GOOGLEPLUSSHARE, "Google+", getResources().getDrawable(com.h4oxer.fuseapp.R.drawable.google));
	     ActionItem twitterItem 	= new ActionItem(TWITTERSHARE, "Twitter", getResources().getDrawable(com.h4oxer.fuseapp.R.drawable.twitter));
	     ActionItem infoItem = new ActionItem(MOREINFO,"Info",getResources().getDrawable(android.R.drawable.ic_menu_info_details));
	     infoItem.setSticky(true);
	     mQuickAction = new QuickAction(this);
	     mQuickAction.addActionItem(infoItem);
	     mQuickAction.addActionItem(fbItem);
	     mQuickAction.addActionItem(googleItem);
	     mQuickAction.addActionItem(twitterItem);
	     
			
	}
	
	private void shareFacebook() {
		if(currentEvent == null || currentDay == null){
    		return;
    	}
		this.facebookLoginAndShare();
		
	}
	
	private void shareTwitter(Event e,String day) {
		String tweetUrl = "https://twitter.com/intent/tweet?text=Awesome, check deze activiteit van %23Fuse : "+e.getTitle()+" aan "+e.getLocation()+" op "+day.toLowerCase(Locale.getDefault())+" om "+e.getDate()+" ! info: &url="
                + fuseUrl;
		Uri uri = Uri.parse(tweetUrl);
		Intent i = new Intent(Intent.ACTION_VIEW, uri);
		this.startActivity(i);
		
	}
	
	private void shareGooglePlus(Event e,String day) {
			int errorCode = GooglePlusUtil.checkGooglePlusApp(this);
			if (errorCode != GooglePlusUtil.SUCCESS) {
			  GooglePlusUtil.getErrorDialog(errorCode, this, 0).show();
			}
		 Intent shareIntent = new PlusShare.Builder(this)
         .setType("text/plain")
         .setText("Awesome, check deze activiteit van Fuse : "+e.getTitle()+" aan "+e.getLocation()+" op "+day+" om "+e.getDate()+" !")
         .setContentUrl(Uri.parse(fuseUrl))
         .getIntent();

     startActivityForResult(shareIntent, 0);
        
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        mPlusClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPlusClient.disconnect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
            } catch (SendIntentException e) {
                mPlusClient.connect();
            }
        }
        // Save the result and resolve the connection failure upon a user click.
        mConnectionResult = result;
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
    	super.onActivityResult(requestCode, responseCode, intent);
  	  
        if (requestCode == REQUEST_CODE_RESOLVE_ERR && responseCode == RESULT_OK) {
        	//google plus response
            mConnectionResult = null;
            mPlusClient.connect();
        }else{
        	if(intent != null){
	        	//facebook response
	        	Session.getActiveSession().onActivityResult(this, requestCode, responseCode, intent);
        	}
        }
    }
    
    

    @Override
    public void onConnected() {
        String accountName = mPlusClient.getAccountName();
        Toast.makeText(this, accountName + " is connected.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDisconnected() {
        Log.d("CALENDARACTIVITY", "disconnected");
    }
    
    private void facebookLoginAndShare(){
    	try {
    	    PackageInfo info = getPackageManager().getPackageInfo(
    	            "com.h4oxer.fuseapp", PackageManager.GET_SIGNATURES);
    	    for (android.content.pm.Signature signature : info.signatures) {
    	        MessageDigest md = MessageDigest.getInstance("SHA");
    	        md.update(signature.toByteArray());
    	        Log.e("MY KEY HASH:",
    	                Base64.encodeToString(md.digest(), Base64.DEFAULT));
    	    }
    	} catch (NameNotFoundException e) {

    	} catch (NoSuchAlgorithmException e) {
    		
    	}
    	// start Facebook Login
		  Session.openActiveSession(this, true, new Session.StatusCallback() {

		    // callback when session changes state
		    @Override
		    public void call(final Session session, SessionState state, Exception exception) {
		    	
		    	if (session.isOpened()) {
		    		// make request to the /me API
		    		Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

		    		  // callback after Graph API response with user object
		    		  @Override
		    		  public void onCompleted(GraphUser user, Response response) {
		    			  if (user != null) {
		    				  publishStory(session);
		    				  Toast.makeText(CalendarActivity.this, "Gedeeld op Facebook, woohoo !",Toast.LENGTH_SHORT).show();
		    				}
		    		  }
		    		});
		    	}

		    }
		  });
    }
    
    private void publishStory(Session session) {
	    	if(currentEvent == null || currentDay == null){
	    		return;
	    	}

		    if (session != null){
	
		        // Check for publish permissions    
		        List<String> permissions = session.getPermissions();
		        if (!isSubsetOf(PERMISSIONS, permissions)) {
		            setPendingPublishReauthorization(true);
		            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS);
		            session.requestNewPublishPermissions(newPermissionsRequest);
		            return;
		        }
	
		        Bundle postParams = new Bundle();
		        postParams.putString("name", "Awesome, check deze activiteit van Fuse : "+currentEvent.getTitle()+" aan "+currentEvent.getLocation()+" op "+currentDay.getDay().toLowerCase(Locale.getDefault())+" om "+currentEvent.getDate()+" !");
		        postParams.putString("caption", "Kiesploeg Fuse - Be Our Spark !");
		        postParams.putString("description", currentEvent.getDescription());
		        postParams.putString("link", this.fuseUrl);
		        Request.Callback callback= new Request.Callback() {
		           
	
					public void onCompleted(Response response) {
		                
		                FacebookRequestError error = response.getError();
		                if (error != null) {
		                    Toast.makeText(getApplicationContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
		                    } else {
		                    	Toast.makeText(CalendarActivity.this, "Gedeeld op Facebook, woohoo !",Toast.LENGTH_SHORT).show();
		                }
		            }
		        };
	
		        Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
		        RequestAsyncTask task = new RequestAsyncTask(request);
		        task.execute();
		    }
	    }
	    
	   

		/**
		 * @return the pendingPublishKey
		 */
		public static String getPendingPublishKey() {
			return PENDING_PUBLISH_KEY;
		}

		/**
		 * @return the pendingPublishReauthorization
		 */
		public boolean isPendingPublishReauthorization() {
			return pendingPublishReauthorization;
		}

		/**
		 * @param pendingPublishReauthorization the pendingPublishReauthorization to set
		 */
		public void setPendingPublishReauthorization(boolean pendingPublishReauthorization) {
			this.pendingPublishReauthorization = pendingPublishReauthorization;
		}

	


	private boolean isSubsetOf(List<String> subset,
			List<String> superset) {
		for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}

}
