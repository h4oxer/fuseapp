package com.h4oxer.fuseapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;
import com.h4oxer.fuseapp.db.MessageDataSource;
/**
 * Push service 
 * @author Incalza Dario
 *
 */
public class GCMIntentService extends GCMBaseIntentService {
	
	public static final String SENDER_ID = "125567492900";
	private static final String tag = "GCMIntentService";
	//private NotificationManager notifyMgr;
	//private static final int NOTIFY_ME_ID=1337;
	
	public GCMIntentService(){
		super(SENDER_ID);

	}

	@Override
	protected void onError(Context arg0, String arg1) {
		Log.d(tag,"onError");
		Toast.makeText(arg0, "Error !", Toast.LENGTH_SHORT).show();

	}

	@Override
	protected void onMessage(Context arg0, Intent arg1) {
		Log.d(tag,"onMessage");
		MessageDataSource database = new MessageDataSource(this);
	    Intent intent = new Intent(this, MainActivity.class);
	    PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

	    // Build notification
	    // Actions are just fake
	    Notification nfy = new Notification(R.drawable.ic_launcher, "Fuse Melding !", System.currentTimeMillis());
	    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	    // Hide the notification after its selected
	    nfy.flags |= Notification.FLAG_AUTO_CANCEL;
	    nfy.vibrate = new long[]{100, 200, 100, 500}; 
	    database.open();
	    database.createMessage(arg1.getStringExtra("title"), arg1.getStringExtra("message"),new Date());
	    database.close();
	    //MessageManager.get().addMessage(arg1.getStringExtra("message"));
	    nfy.setLatestEventInfo(this, "Fuse", arg1.getStringExtra("message"),pIntent);
	    notificationManager.notify(0, nfy);
    
		
	}
	
	@Override
	protected void onRegistered(Context arg0, String arg1) {
		Log. d(tag, "onRegistered");
		HttpURLConnection connection;
	       OutputStreamWriter request = null;

	            URL url = null;   
	            String response = null;         
	            String parameters = "deviceToken="+arg1;   

	            try
	            {
	                url = new URL("http://fuse.vtk.be/postToken.php");
	                connection = (HttpURLConnection) url.openConnection();
	                connection.setDoOutput(true);
	                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	                connection.setRequestMethod("POST");    
	                request = new OutputStreamWriter(connection.getOutputStream());
	                request.write(parameters);
	                request.flush();
	                request.close();            
	                String line = "";               
	                InputStreamReader isr = new InputStreamReader(connection.getInputStream());
	                BufferedReader reader = new BufferedReader(isr);
	                StringBuilder sb = new StringBuilder();
	                while ((line = reader.readLine()) != null)
	                {
	                    sb.append(line + "\n");
	                }
	                // Response from server after login process will be stored in response variable.                
	                response = sb.toString();
	                // You can perform UI operations here
	                Toast.makeText(this,"Message from Server: \n"+ response, Toast.LENGTH_LONG).show();             
	                isr.close();
	                reader.close();

	            }
	            catch(IOException e)
	            {
	                Toast.makeText(this,"IOException",Toast.LENGTH_LONG).show();
	            }
		Toast.makeText(arg0, "Registered ! : "+arg1, Toast.LENGTH_SHORT).show();


	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		Log.d(tag,"onUnregistered");
		Toast.makeText(arg0, "Unregistered !", Toast.LENGTH_SHORT).show();


	}

}
