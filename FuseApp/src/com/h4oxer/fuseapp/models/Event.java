/**
 * 
 */
package com.h4oxer.fuseapp.models;


/**
 * 
 * Model class stelt een event voor.
 * 
 * @author Incalza Dario
 *
 */
public class Event {
	
	private final String title,description,location;
	private final String date;
	
	public Event(String title,String description,String location,String date){
		this.title = title;
		this.description = description;
		this.location = location;
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getLocation() {
		return location;
	}

	

}
