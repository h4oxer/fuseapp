package com.h4oxer.fuseapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Database interface voor database connectie
 * @author Incalza Dario
 *
 */
public class DbHelper extends SQLiteOpenHelper{
    
	public static String dbName = "messages.db";
	public static final int dbVersion = 8;
	public static final String TABLE_MESSAGES = "_messages";
	public static final String COLUMN_TITLE="_title";
	public static final String COLUMN_BODY="_body";
	public static final String COLUMN_ID="_id";
	public static final String COLUMN_DATE="_date";
	
	public static final String CREATE_TABLE_MESSAGES = "CREATE TABLE "
	        + TABLE_MESSAGES + " (" + COLUMN_ID
	        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_TITLE+" TEXT NOT NULL, " + COLUMN_BODY
	        + " TEXT NOT NULL UNIQUE, "+ COLUMN_DATE+" INTEGER NOT NULL);";
	
	public DbHelper(Context c){
		super(c,dbName,null,dbVersion);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_MESSAGES);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DbHelper.class.getName(),
		        "Upgrading database from version " + oldVersion + " to "
		            + newVersion + ", which will destroy all old data");
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
		    onCreate(db);
		
	}
	 
}