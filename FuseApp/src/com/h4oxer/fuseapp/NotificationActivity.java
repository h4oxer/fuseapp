package com.h4oxer.fuseapp;


import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.h4oxer.fuseapp.db.MessageDataSource;
import com.h4oxer.fuseapp.models.Message;
import com.h4oxer.fuseapp.utils.MessageListViewAdapter;
import com.h4oxer.fuseapp.utils.Typefaces;



public class NotificationActivity extends Activity implements OnItemClickListener{
	
	private ListView list;
	private List<Message> data;
	private MessageListViewAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		list = (ListView) findViewById(R.id.notifications);
		data = this.getMessages();
		
		list.setSelector(R.layout.listselector);
		adapter = new MessageListViewAdapter(getApplicationContext(), R.layout.message_row, data);
		list.setAdapter(adapter);
		list.setOnItemClickListener(this);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if(prefs.getBoolean("firstBoot", true)){
			Toast.makeText(this, "ProTip : Klik op een bericht om meer te lezen!", Toast.LENGTH_LONG).show();
			Editor edit = prefs.edit();
			edit.putBoolean("firstBoot", false);
			edit.commit();
		}
		
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		data = getMessages();
		adapter.notifyDataSetChanged();
	}
	
	private List<Message> getMessages(){
		if(list != null){
			list.setVisibility(View.GONE);
		}
		ProgressBar pBar = (ProgressBar) findViewById(R.id.progressMessage);
		pBar.setIndeterminate(false);
		pBar.setVisibility(View.VISIBLE);
		MessageDataSource database = new MessageDataSource(this);
		database.open();
		List<Message> messages = database.getMessages();
		database.close();
		pBar.setVisibility(View.GONE);
		if(messages == null || messages.size() == 0){
			TextView tv = (TextView) findViewById(R.id.notifAlert);
			Typeface face=Typefaces.get(this,"fonts/Roboto-Regular.ttf");
			tv.setTypeface(face);
			tv.setVisibility(View.VISIBLE);
		}else{
			list.setVisibility(View.VISIBLE);
		}
		return messages;
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		if(data != null){
			Message m = data.get(position);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(m.getTitle());
			builder.setMessage("Verzonden : "+m.getTimeStampAsString()+"\n\n"+m.getSubject());
			builder.setCancelable(false);
			
			builder.setPositiveButton("Ok !",new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		
	}

}
