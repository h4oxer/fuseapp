/**
 * 
 */
package com.h4oxer.fuseapp.utils;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.h4oxer.fuseapp.R;
import com.h4oxer.fuseapp.models.Message;


/**
 * 
 * Een list adapter om messages te parsen en weer te geven.
 * 
 * @author Incalza Dario
 *
 */
public class MessageListViewAdapter extends ArrayAdapter<Message> {
	
	private final List<Message> objects;
	private Context c;
	
	public MessageListViewAdapter(Context context, int textViewResourceId,
			List<Message> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.c = context;
	}
	
	 class MessageHolder{
		 TextView title,description,date;
	}
	
	/**
	 * Return a single item view in the listview. 
	 */
	public synchronized View getView(int position, View convertView, ViewGroup parent) {
        MessageHolder holder = null;
        Message m = objects.get(position);
 
        LayoutInflater mInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.message_row, null);
            holder = new MessageHolder();
            holder.title = (TextView) convertView.findViewById(R.id.subject);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        } else
            holder = (MessageHolder) convertView.getTag();
        	holder.title.setText(ellipsize(m.getTitle(),20));
        	holder.description.setText(ellipsize(m.getSubject(),50));
        	holder.date.setText(m.getTimeStampAsString());
        
        	Typeface face=Typefaces.get(c,"fonts/Roboto-Regular.ttf");
        	holder.title.setTypeface(face);
        	
        	Typeface face2=Typefaces.get(c,"fonts/Roboto-Light.ttf");
        	holder.description.setTypeface(face2);
        	holder.date.setTypeface(face2);
        return convertView;
    }
	
	
	public static String ellipsize(String text, int max) {

	    if (textWidth(text) <= max)
	        return text;

	    // Start by chopping off at the word before max
	    // This is an over-approximation due to thin-characters...
	    int end = text.lastIndexOf(' ', max - 3);

	    // Just one long word. Chop it off.
	    if (end == -1)
	        return text.substring(0, max-3) + "...";

	    // Step forward as long as textWidth allows.
	    int newEnd = end;
	    do {
	        end = newEnd;
	        newEnd = text.indexOf(' ', end + 1);

	        // No more spaces.
	        if (newEnd == -1)
	            newEnd = text.length();

	    } while (textWidth(text.substring(0, newEnd) + "...") < max);

	    return text.substring(0, end).replaceAll("&#039;", "'").replaceAll("&amp;", "&").replaceAll("&quot;", "\"") + "...";
	}
	
	private final static String NON_THIN = "[^iIl1\\.,']";

	
	private static int textWidth(String str) {
	    return (int) (str.length() - str.replaceAll(NON_THIN, "").length() / 2);
	}
	

}
